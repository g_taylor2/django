from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.db import models
from phone_field import PhoneField

import os

# Create your models here.

# --------------------------------------------------- SHOPPING ---------------------------------------------------------

class Category(models.Model):
    name = models.CharField(max_length=200, db_index=True, help_text="name of product")
    slug = models.SlugField(max_length=200, db_index=True, unique=True, help_text="slug of product")

    class Meta:
        ordering = ('name',)
        verbose_name = 'category'
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    #def get_absolute_url(self):
        #return reverse('shop:product_list_by_category', args=[self.slug])

class Product(models.Model):
    category        = models.ForeignKey(
                      Category,
                      related_name='products',
                      on_delete=models.CASCADE,
                      help_text="the product category")

    name            = models.CharField(
                      max_length=200,
                      db_index=True,
                      help_text="product name")

    slug            = models.SlugField(
                      max_length=200,
                      db_index=True,
                      help_text="product slug")

    image           = models.ImageField(
                      blank=True,
                      help_text="image file url")

    description     = RichTextUploadingField(
                      blank=True,
                      help_text="product decription")

    price           = models.DecimalField(
                      max_digits=6,
                      decimal_places=2,
                      help_text="product price in GBP incl. VAT")

    mass            = models.DecimalField(
                      default=False,
                      max_digits=5,
                      decimal_places=1,
                      help_text="product mass in grams")

    stock           = models.PositiveIntegerField(
                      help_text="available stock of product")

    available       = models.BooleanField(
                      default=True,
                      help_text="product availability")

    created         = models.DateTimeField(
                      auto_now_add=True,
                      help_text="date product added online")

    updated         = models.DateTimeField(
                      auto_now=True,
                      help_text="date product updated online")

    class Meta:
        ordering = ('created',)
        index_together = (('id', 'slug'),)

    def __str__(self):
        return self.name

    #def get_absolute_url(self):
       # return reverse('shop: product_detail', args=[self.id, self.slug])


# -------------------------------------------------- PHOTO--------------------------------------------------------------

# def get_photo_path():
#     return str(os.path.abspath(os.path.join(os.path.dirname(__file__))))

class Photo(models.Model):

    photo_name        = models.CharField(
                        max_length=200,
                        null=True,
                        blank=True,
                        unique=True,
                        help_text="Title of photo")

    photo_path        = models.FilePathField(
                        unique=True,
                        max_length=128,
                        path="G:\All Pics & Videos\Witney",
                        #path=settings.FILE_PATH_FIELD_DIRECTORY,
                        help_text="Path to photo stored on disk")


    photo_format      = models.CharField(
                        max_length=5,
                        null=True,
                        blank=True,
                        help_text="File format of photo")

    photo_size_MB     = models.CharField(
                        max_length=10,
                        null=True,
                        blank=True,
                        help_text="File size of photo on disk in MB")

    photo_created     = models.DateTimeField(
                        null=True,
                        blank=True,
                        help_text="File size of photo on disk in MB")

    photo_location    = models.CharField(
                        max_length=100,
                        null=True,
                        blank=True,
                        help_text="Location where photo was captured")

    photo_description = models.CharField(
                        max_length=200,
                        null=True,
                        blank=True,
                        help_text="Photo tags separated by a semicolon")

    photo_slug        = models.SlugField(
                        max_length=200,
                        unique=True,
                        help_text="Uses the title of a photo to generate a URL slug")

    # objects = PhotoQuerySet.as_manager()  # converts queryset into manage form

    class Meta:
        verbose_name_plural = "Photographs"
        ordering = ["-photo_created"]

    def __str__(self):
        return self.photo_name

# -------------------------------------------------- VIDEO -------------------------------------------------------------

def get_video_path():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), 'data'))

class Video(models.Model):

    video_name        = models.CharField(
                        max_length=200,
                        null=True,
                        blank=True,
                        unique=True,
                        help_text="Title of video")

    video_path        = models.FilePathField(
                        unique=True,
                        max_length=128,
                        path="G:\All Pics & Videos\Witney",
                        help_text="Path to video stored on disk")

    video_format      = models.CharField(
                        max_length=5,
                        null=True,
                        blank=True,
                        help_text="File format of video written as '.' followed by the file extension e.g. '.mov'")

    video_size_MB     = models.CharField(
                        max_length=10,
                        null=True,
                        blank=True,
                        help_text="File size of video on disk in MB")

    video_created     = models.DateTimeField(
                        max_length=10,
                        null=True,
                        blank=True,
                        help_text="Date and time when video was created")

    video_location    = models.CharField(
                        max_length=100,
                        null=True,
                        blank=True,
                        help_text="Primary location where video was captured")

    video_description = models.CharField(
                        max_length=200,
                        null=True,
                        blank=True,
                        help_text="Video tags separated by a semicolon")

    video_slug        = models.SlugField(
                        max_length=200,
                        unique=True,
                        help_text="Uses the title of a video to generate a URL slug")

    class Meta:
        ordering = ["-video_created"]

    def __str__(self):
        return self.video_name


# -------------------------------------------------- BLOG --------------------------------------------------------------

class EntryQuerySet(models.QuerySet):
    def published(self):
        return self.filter(publish=True)

class Entry(models.Model):
    title     = models.CharField(
                max_length=200,
                help_text="Title of blog entry limited to 200 characters")

    topic     = models.CharField(
                max_length=100,
                help_text="The general topic of the blog entry")

    body      = RichTextUploadingField(
                null=True,
                blank=True)

    publish   = models.BooleanField(
                default=True,
                help_text="Check box to publish blog entry. Leave unchecked to avoid publishing blog entry")

    slug      = models.SlugField(
                max_length=200,
                unique=True,
                help_text="Uses the title of an article to generate a URL slug")

    created   = models.DateTimeField(
                auto_now_add=True,
                help_text="Creation date of blog entry")

    modified  = models.DateTimeField(
                auto_now=True,
                help_text="Date and time when blog post was last modified")

    objects   = EntryQuerySet.as_manager() # converts queryset into manage form

    class Meta:
        verbose_name = "Blog Entry"
        verbose_name_plural = "Blog Entries"
        ordering = ["-created"]

    def __str__(self):
        return self.title

    # def get_absolute_url(self):
    #     return reverse("entry_detail", kwargs={'slug': self.slug})

# -------------------------------------------------- USER --------------------------------------------------------------

class UserProfile(models.Model):
    user           = models.ForeignKey(
                     User,

                     default=None,
                     null=True,
                     on_delete=models.CASCADE)

    email          = models.CharField(
                     max_length=50,
                     blank=True)

    password       = models.CharField(
                     max_length=50,
                     blank=True)

    house_number   = models.CharField(
                     max_length=30,
                     blank=True)

    street_address = models.CharField(
                     max_length=100,
                     null=True,
                     blank=True)

    build          = models.CharField(
                     max_length=50,
                     null=True,
                     blank=True)

    postal_code    = models.CharField(
                     max_length=10,
                     null=True,
                     blank=True)

    phone          = PhoneField(
                     blank=True,
                     help_text='Contact phone number - landline or mobile')


    def __str__(self):
        return self.user.username

def create_profile(sender, **kwargs):
    # if user object has been created
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile, sender=User)