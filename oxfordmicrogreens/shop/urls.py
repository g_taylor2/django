from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.auth.views import LoginView

app_name = 'shop'

urlpatterns = [
    # /shop/
    path('', views.product_list, name='product_list'),

    # /shop/home/
    path('home/', views.home, name='home'),

    # /shop/about/
    path('about/', views.about, name='about'),

    # /shop/contact/
    path('contact/', views.contact, name='contact'),

    # /shop/email/
    path('email/', views.email, name='email'),

    # /shop/email/success/
    path('email/success/', views.success, name='success'),

    # /shop/blog/
    path('blog/', views.blog, name='blog'),

    # /shop/blog/<slug>/
    path('blog/<slug>', views.blog_detail, name='blog_detail'),

    # /shop/delivery/
    path('delivery/', views.delivery, name='delivery'),

    # /shop/team/
    path('team/', views.team, name='team'),

    # /shop/products/microgreens/
    path('products/microgreens/', views.microgreens, name='microgreens'),

    # /shop/products/microgreens/
    path('products/microgreens/<slug>', views.microgreen_detail, name='microgreen_detail'),

    # /shop/products/growth-cabinets/
    path('products/growth-cabinets/', views.growth_cabinets, name='growth_cabinets'),

    # /shop/register/
    path('register/', views.register, name='register'),

    # /shop/profile/
    path('profile/', views.profile, name='profile'),

    # /shop/profile/edit/
    path('profile/edit/', views.edit_profile, name='edit_profile'),

    # /shop/profile/change-password/
    path('profile/change-password/', views.change_password, name='change_password'),

    # /shop/login/
    path('login/', LoginView.as_view(template_name='shop/login.html'), name="login_user"),

    # /shop/login_successful/
    path('login_successful/', views.login_successful, name='login_successful'),

    # /shop/logout/
    #path('logout/', views.logout_user, name='logout_user'),

# /shop/login_successful/
    path('test/', views.test, name='test'),

    # '/'
    path('', include('django.contrib.auth.urls'))

]
