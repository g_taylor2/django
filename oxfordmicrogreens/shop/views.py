from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator
from django.views.generic import TemplateView
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm

from django.contrib.auth import (authenticate,
                                 login,
                                 update_session_auth_hash)

from .forms import (RegistrationForm,
                    LoginForm,
                    EditProfileForm)

from .models import (Category,
                     Product,
                     Entry,
                     UserProfile)


# Create your views here.

def product_list(request, category_slug=None):
    category = None
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        products = products.filter(category=category)
    return render(request, 'shop/Products/list.html', {'category': category, \
                                                        'categories': categories, \
                                                        'products': products})


def product_detail(request, id, slug):
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    cart_product_form = CartAddProductForm()
    return render(request,
                  'shop/Products/detail.html',
                  {'product': product,
                   'cart_product_form': cart_product_form})


def home(request):
    return render(request, 'shop/home.html')


def about(request):
    return render(request, 'shop/about.html')


def blog(request):
    posts = Entry.objects.all()
    paginator = Paginator(posts, 2)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request, 'shop/blog.html', {'posts': posts})


def blog_detail(request, slug):
    post = get_object_or_404(Entry, slug=slug)
    return render(request, 'shop/blog_detail.html', {'post': post})


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('/shop/profile/')

        else:
            return redirect('shop/change-password/')

    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'shop/change_password.html', args)


def contact(request):
    return render(request, 'shop/contact.html')


def delivery(request):
    return render(request, 'shop/delivery.html')


def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('/shop/profile/')

    else:
        form = EditProfileForm(instance=request.user)
        args = {'form': form}
        return render(request, 'shop/edit_profile.html', args)


def email(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['taylor.graham@zohomail.eu'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('success')
    return render(request, 'shop/email.html', {'form': form})


def growth_cabinets(request):
    products = Product.objects.filter(available=True)
    return render(request, 'shop/growth_cabinets.html', {'products': products})

def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('shop:home')

    else:
        form = LoginForm()

        args = {'form': form}
        return render(request, 'shop/login.html', args)


def login_successful(request):
    return render(request, 'shop/login_successful.html')


def logout_user(request):
    return render(request, 'shop/logout.html')


def microgreens(request):
    products = Product.objects.filter(available=True)
    return render(request, 'shop/microgreens.html', {'products': products})


def microgreen_detail(request, slug):
    microgreen = get_object_or_404(Product, slug=slug)
    return render(request, 'shop/microgreen_detail.html', {'microgreen': microgreen})


def profile(request):
    user = UserProfile.objects.all()
    return render(request, 'shop/profile.html', {'user': request.user})


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/shop/login/')

    else:
        form = RegistrationForm()

    args = {'form': form}
    return render(request, 'shop/registration_form.html', args)


def success(request):
    return render(request, 'shop/success.html')


def team(request):
    return render(request, 'shop/team.html')


class HomeView(TemplateView):
    template_name = 'shop/registration_form.html'

    def get(self, request):
        form = HomeForm()
        return render(request, self.template_name, {'form': form})

    # process form data
    def post(self, request):
        form = HomeForm(request.POST)

        if form.is_valid():
            street_address = form.save(commit=False)
            street_address.user = request.user
            street_address.save()
            text = form.cleaned_data['street_address']
            form = HomeForm()
            return redirect('shop:home')

        args = {'form': form, 'text': text}

        return render(request, self.template_name, args)

