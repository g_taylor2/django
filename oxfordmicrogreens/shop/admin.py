from django.contrib import admin
from .models import (Category,
                     Product,
                     Entry,
                     Photo,
                     Video,
                     UserProfile)


# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}
    

class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'category', 'mass', 'price', 'stock', 'available', 'created', 'updated']
    list_filter = ['available', 'created', 'updated', 'category']
    # Entries in list_editable must be listed in list_display above since only diplayed entries can be edited
    list_editable = ['price', 'mass', 'stock', 'available']
    prepopulated_fields = {'slug': ('name',)}


class EntryAdmin(admin.ModelAdmin):
    list_display        = ("title", "topic", "created", "modified")
    prepopulated_fields = {"slug": ("title",)}


class PhotoAdmin(admin.ModelAdmin):
    list_display        = ("photo_name", "photo_path", "photo_format", "photo_size_MB", "photo_created", "photo_location",
                           "photo_description")
    prepopulated_fields = {"photo_slug": ("photo_name",)}


class VideoAdmin(admin.ModelAdmin):
    list_display        = ("video_name", "video_path", "video_format", "video_size_MB", "video_created", "video_location",
                           "video_description")
    prepopulated_fields = {"video_slug": ("video_name",)}


class UserProfileAdmin(admin.ModelAdmin):
    list_display        = ("user", "email", "house_number", "street_address", "postal_code", "build")

    def email(self, obj):
        return(obj.email)

    def build(self, obj):
        return(obj.build)

    def house_number(self, obj):
        return(obj.house_number)

    def street_address(self, obj):
        return (obj.steet_address)

    def postal_code(self, obj):
        return (obj.postal_code)


# class ProfileInline(admin.StackedInline):
#     model = UserProfile
#     can_delete = False
#     verbose_name_plural = 'Profile'
#     fk_name = 'user'
#
# class CustomUserAdmin(UserAdmin):
#     inlines = (ProfileInline,)
#     list_display = ("username", 'email', 'first_name', 'last_name', 'is_staff', 'build',)
#     list_select_related = ('profile',)
#
#     def get_build(self, instance):
#         return instance.profile.build
#
#     get_build.short_description = 'Build'
#     def get_inline_instances(self, request, obj=None):
#         if not obj:
#             return list()
#         return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Entry, EntryAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
