from django import forms
from django.contrib.auth.models import User
from .models import UserProfile
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

class RegistrationForm(UserCreationForm):
    # add widget for asterix display

    email          = forms.EmailField(
                                      required=True,
                                      help_text="*Required Field")
    #
    # house_number   = forms.CharField( initial="",
    #                                   required=True,
    #                                   help_text="*Required Field"
    #                                )
    #
    # street_address = forms.CharField( initial="",
    #                                   required=True,
    #                                   help_text="*Required Field"
    #                          )
    #
    # build          = forms.CharField( initial="",
    #                                   required=True,
    #                                   help_text="*Required Field"
    #                         )
    #
    # postal_code    = forms.CharField( initial="",
    #                               required=True,
    #                               help_text="*Required Field"
    #                               )

    class Meta:
        model   = User
        fields  = {
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2'
        }

    field_order = ['username',
                   'first_name',
                   'last_name',
                   'email',
                   'password1',
                   'password2']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class LoginForm(UserCreationForm):

    class Meta:
        model   = UserProfile
        fields  = {
            'user',
            'password1',
        }

    field_order = ['user',
                   'password1',
        ]


class EditProfileForm(UserChangeForm):

    class Meta:
        model = User
        fields = {
            'email',
            'first_name',
            'last_name',
            'password',
        }


class HomeForm(forms.ModelForm):
    street_address = forms.CharField()
    #user = forms.CharField()

    class Meta:
        model = UserProfile
        fields = ('street_address',)
