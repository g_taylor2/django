from django.contrib import admin
# from django.markdown.admin import MarkdownModelAdmin
# from markdownx.admin import MarkdownxModelAdmin
from django.db import models

# from django_markdown.admin import MarkdownModelAdmin
# from django_markdown.widgets import AdminMarkdownWidget
from django.db.models import TextField
from markdownx.admin import MarkdownxModelAdmin

from .models import Photo, Entry, Video
# Register your models here.

class EntryAdmin(admin.ModelAdmin):
    list_display        = ("title", "topic", "created", "modified")
    prepopulated_fields = {"slug": ("title",)}

class PhotoAdmin(admin.ModelAdmin):
    list_display        = ("photo_name", "photo_path", "photo_format", "photo_size_MB", "photo_created", "photo_location",
                           "photo_description")
    prepopulated_fields = {"photo_slug": ("photo_name",)}

class VideoAdmin(admin.ModelAdmin):
    list_display        = ("video_name", "video_path", "video_format", "video_size_MB", "video_created", "video_location",
                           "video_description")
    prepopulated_fields = {"video_slug": ("video_name",)}

admin.site.register(Entry, EntryAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Video, VideoAdmin)
