from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from species_db.models import Client, Specie, Mine, Habit, Family, Planting_Window, Distribution, Climate
import pandas as pd

class Command(BaseCommand):
    help = 'Get data from species database'

    def add_arguments(self, parser):
        parser.add_argument('file', type=str, help='source file directory')

        #def handle2(self, parser):
        #parser.add_argument('sheet name', nargs='+', type=str, help='source file sheet name')

    def handle(self, **kwargs):
        """

        :param args:
        :param kwargs: file name of source file (Note: use double-slashes for file directory)
        :return: successfully loaded database file
        """
        file1 = kwargs['file']
        # Sheet_name = kwargs['sheet name']

        # "C:\\Users\\BCE-Database\\Documents\\Australian_Species.xlsx"   # example source file for species data

        sheet = "Sheet1"
        # Load source file
        xl = pd.ExcelFile(file1)
        # Read sheet of workbook source file - specify sheet name
        df = xl.parse(sheet)
        #df = df.applymap(str)
        # Read all data columns and rows from workbook
        # df1 = df.iloc[0:145, 0:116]
        # Get data from below header row in file
        #df2 = df1[1:]
        # Loop through each row of data in pandas dataframe and add object to specific model class in django database
        for index, row in df.iterrows():
            specie_name                                        = row[0]
            synonym                                            = row[2]
            family                                             = row[3]
            common_name                                        = row[4]
            habit                                              = row[1]
            distribution_gps_file_available                    = row[5]
            distribution_area                                  = row[6]
            economical_uses                                    = row[7]
            continent                                          = row[9]
            # native_naturalised_country                       = row[9]
            exotic_countries                                   = row[10]
            invasive_countries                                 = row[11]
            country_state_native                               = row[12]
            country_state_invasive                             = row[13]
            IBRA_region                                        = row[14]
            IBRA_subregion                                     = row[15]
            ecotype                                            = row[16]
            native_biome                                       = row[17]
            ngo                                                = row[18]
            client_name                                        = row[19]
            mine_name                                          = row[20]
            mine_lat                                           = row[21]
            mine_lng                                           = row[22]
            seed_length_in_mm                                  = row[23]
            seed_width_in_mm                                   = row[24]
            seed_sphericity_ratio                              = row[25]
            seed_viability_Kew                                 = row[26]
            seed_viability_Kew_std_dev                         = row[27]
            seed_viability_SSA                                 = row[28]

            seed_viability_combined_decimal                    = row[30]
            seed_viability_combined_decimal_stdev              = row[31]
            mass_of_1000_seeds_in_g_Kew                        = row[32]
            mass_of_1000_seeds_in_g_Kew_stdev                  = row[33]
            mass_of_1000_seeds_in_g_SSA                        = row[34]
            mass_of_1000_seeds_in_g_SSA_stdev                  = row[35]
            mass_of_1000_seeds_in_g_combined                   = row[36]
            mass_of_1000_seeds_in_g_combined_stdev             = row[37]
            seeds_per_g_Kew                                    = row[38]
            seeds_per_g_stdev_Kew                              = row[39]
            mass_per_seed_g                                    = row[40]
            mass_per_seed_g_stdev                              = row[41]
            viable_seeds_per_kg_Kew                            = row[42]
            seed_volume_per_1000_seeds_mL                      = row[43]
            seed_storage_behaviour                             = row[44]
            seed_dormancy                                      = row[45]
            seeds_currently_in_BCE_storage                     = row[46]
            seeds_available_stratified                         = row[47]
            seed_germination_type                              = row[48]
            seed_germination_time                              = row[49]
            seed_pretreatment_tested                           = row[50]
            growing_media_tested                               = row[51]
            best_seed_pretreatment                             = row[52]
            seed_pretreatment_cold_wks                         = row[53]
            seed_pretreatment_warm_wks                         = row[54]
            # planting_density_kg_per_ha                       = row[55]
            seed_predation                                     = row[56]
            planting_window                                    = row[57]
            life_cycle                                         = row[58]
            light_requirement_germination                      = row[59]
            mean_annual_temperature_min                        = row[60]
            mean_annual_temperature_max                        = row[61]
            max_temperature_hottest_month                      = row[62]
            max_temperature_coldest_month                      = row[63]
            frost_sensitivity                                  = row[64]
            mean_annual_rainfall_min                           = row[65]
            mean_annual_rainfall_max                           = row[66]
            min_altitude_masl                                  = row[67]
            max_altitude_masl                                  = row[68]
            soil_pH                                            = row[69]
            preferred_soil_type                                = row[70]
            nutrient_requirement                               = row[71]
            salt_tolerance                                     = row[72]
            carbon_sequestration_potential                     = row[73]
            growth_rate_min                                    = row[74]
            growth_rate_max                                    = row[75]
            ecological_succession                              = row[76]
            expected_survival_after_3_years                    = row[77]
            IUCN_status                                        = row[78]
            journal_references                                 = row[79]
            web_data                                           = row[80]
            #remarks                                           = row[81]
            #comments                                          = row[82]

            seed_supplier                                      = row[83]
            cost_of_seed_per_kg_in_USD                         = row[84]
            seed_retail_link                                   = row[85]
            climate_class                                      = row[86]
            comments_combined                                  = row[87]
            # seed_planting_ratio                              = row[86]
            # kg_of_seed_required_per_ha                       = row[87]
            # kg_of_seed_required_for_project                  = row[88]

            # specimen_habit_image                             = row[89]
            # specimen_leaves_image                            = row[90]
            # specimen_seeds_image                             = row[91]
            # specimen_flowers_image                           = row[92]

            # indicate spreadsheet row number currently being handled
            print('<---> ROW: ' + str(index+2))

            try:
                p = Planting_Window.objects.get(dates=planting_window)
                print(str(p) + " - Planting Window already exists")
            except ObjectDoesNotExist:
                p = Planting_Window(dates=planting_window)
                # Save now so p can participate in MtoM relationships later
                p.save()
                print(str(p) + " - Planting Window added to database")

            # Try get existing Habit object or create a new one
            try:
                h = Habit.objects.get(growth_habit=habit)
                print(str(h) + " - Habit already exists")
            except ObjectDoesNotExist:
                h = Habit(growth_habit=habit)
                # Save now so h can participate in MtoM relationships later
                h.save()
                print(str(h) + " - Habit added to database")

            # Try get existing Family object or create a new one
            try:
                f = Family.objects.get(family=family)
                print(str(f) + " - Family already exists")
            except ObjectDoesNotExist:
                f = Family(family=family)
                # Save now so f can participate in MtoM relationships later
                f.save()
                print(str(f) + " - Family added to database")

            try:
                cl = Climate.objects.get(climate_class=climate_class)
                print(str(cl) + " - Climate Class already exists")
            except ObjectDoesNotExist:
                cl = Climate(climate_class=climate_class,
                             mean_annual_temperature_min=mean_annual_temperature_min,
                             mean_annual_temperature_max=mean_annual_temperature_max,
                             max_temperature_hottest_month=max_temperature_hottest_month,
                             max_temperature_coldest_month=max_temperature_coldest_month,
                             mean_annual_rainfall_min=mean_annual_rainfall_min,
                             mean_annual_rainfall_max=mean_annual_rainfall_max,
                             min_altitude_masl=min_altitude_masl,
                             max_altitude_masl=max_altitude_masl,
                             )
                # Save now so p can participate in MtoM relationships later
                cl.save()
                print(str(cl) + " - Climate Class added to database")

            # Try get existing Specie object or create a new one
            try:
                s = Specie.objects.get(species_name=specie_name)
                print(str(s) + " - Species already exists")
            except ObjectDoesNotExist:
                s = Specie(species_name=specie_name,
                           synonym=synonym,
                           common_name=common_name,
                           economical_uses=economical_uses,
                           ecotype=ecotype,
                           expected_survival_after_3_years=expected_survival_after_3_years,
                           mass_per_seed_g=mass_per_seed_g,
                           mass_per_seed_g_stdev=mass_per_seed_g_stdev,
                           viable_seeds_per_kg_Kew=viable_seeds_per_kg_Kew,
                           seed_volume_per_1000_seeds_mL=seed_volume_per_1000_seeds_mL,
                           mass_of_1000_seeds_in_g_Kew=mass_of_1000_seeds_in_g_Kew,
                           mass_of_1000_seeds_in_g_Kew_stdev=mass_of_1000_seeds_in_g_Kew_stdev,
                           mass_of_1000_seeds_in_g_SSA=mass_of_1000_seeds_in_g_SSA,
                           mass_of_1000_seeds_in_g_SSA_stdev=mass_of_1000_seeds_in_g_SSA_stdev,
                           mass_of_1000_seeds_in_g_combined=mass_of_1000_seeds_in_g_combined,
                           mass_of_1000_seeds_in_g_combined_stdev=mass_of_1000_seeds_in_g_combined_stdev,
                           seed_storage_behaviour=seed_storage_behaviour,
                           seed_dormancy=seed_dormancy,
                           seed_viability=seed_viability_combined_decimal,
                           native_biome=native_biome,
                           seed_length_in_mm=seed_length_in_mm,
                           seed_width_in_mm=seed_width_in_mm,
                           seed_sphericity_ratio=seed_sphericity_ratio,
                           seed_viability_Kew=seed_viability_Kew,
                           seed_viability_Kew_std_dev=seed_viability_Kew_std_dev,
                           seed_viability_SSA=seed_viability_SSA,
                           seed_viability_combined_decimal=seed_viability_combined_decimal,
                           seed_viability_combined_decimal_stdev=seed_viability_combined_decimal_stdev,
                           seeds_per_g_Kew=seeds_per_g_Kew,
                           seeds_per_g_stdev_Kew=seeds_per_g_stdev_Kew,
                           seed_germination_time=seed_germination_time,
                           seed_pretreatment_tested=seed_pretreatment_tested,
                           growing_media_tested=growing_media_tested,
                           best_seed_pretreatment=best_seed_pretreatment,
                           seed_pretreatment_cold_wks=seed_pretreatment_cold_wks,
                           seed_pretreatment_warm_wks=seed_pretreatment_warm_wks,
                           seeds_currently_in_BCE_storage=seeds_currently_in_BCE_storage,
                           seeds_available_stratified=seeds_available_stratified,
                           seed_germination_type=seed_germination_type,
                           life_cycle=life_cycle,
                           light_requirement_germination=light_requirement_germination,
                           frost_sensitivity=frost_sensitivity,
                           preferred_soil_type=preferred_soil_type,
                           soil_pH=soil_pH,
                           nutrient_requirement=nutrient_requirement,
                           salt_tolerance=salt_tolerance,
                           carbon_sequestration_potential=carbon_sequestration_potential,
                           seed_predation=seed_predation,
                           growth_rate_min=growth_rate_min,
                           growth_rate_max=growth_rate_max,
                           ecological_succession=ecological_succession,
                           IUCN_status=IUCN_status,
                           journal_references=journal_references,
                           web_data=web_data,
                           comments_combined=comments_combined,
                           seed_supplier=seed_supplier,
                           cost_of_seed_per_kg_in_USD=cost_of_seed_per_kg_in_USD,
                           seed_retail_link=seed_retail_link)
                # Save now so s can participate in MtoM relationships later
                s.save()
                print(str(s) + " - Species added to database")

            if ',' in continent and '.' not in continent:
                # One or more semicolons found in client_name
                print(continent + " - Species occurs across more than one continent " + "(" + str(
                    continent) + ")" + " found")

                # Semicolon found, so split client names based on semicolon
                split_continent = continent.split(',')

                # Iterate over each client element in client_name array
                for region in split_continent:
                    # Try get existing client objects or create a new one
                    try:
                        # Remove all white spaces from client names and overwrite 'client' variable
                        region = region.lstrip()
                        region = region.rstrip()
                        # Get list of all continent objects
                        d = Distribution.objects.get(continent=region)
                        # Save and add MtoO relationship of distribution to species
                        d.species.add(s)
                        print(str(d) + " - Distribution already exists")
                    # Distribution objects do not exist, so continue here
                    except ObjectDoesNotExist:
                        # Load Distribution objects continent
                        d = Distribution(continent=region,
                                         distribution_gps_file_available=distribution_gps_file_available,
                                         distribution_area=distribution_area,
                                         exotic_countries=exotic_countries,
                                         invasive_countries=invasive_countries,
                                         country_state_native=country_state_native,
                                         country_state_invasive=country_state_invasive,
                                         IBRA_region=IBRA_region,
                                         IBRA_subregion=IBRA_subregion)

                        # Save Distribution objects
                        d.save()
                        print(str(d) + " - Distribution added to database")
                        # Save and add MtoO relationship of distribution to species
                        d.species.add(s)
                        print(str(s) + " - Added to distribution " + str(d))

                # Check if fullstop has been used as a delimiter for continent
            elif '.' in continent:
                # Fullstop(s) have been used incorrectly as delimiters, so report this
                print("Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1) +
                      ": Invalid delimiter used")
                print("Hint: A period might have been used as a delimiter instead of a semicolon...")
                # Terminate program
                raise SystemExit(0)
                # For every other data entry scenario, continue here
            else:
                # Create a list from client name to check list later for emptiness
                continent_list = [continent]

                if continent_list:
                    for continent in continent_list:
                        continent = continent.lstrip()
                        continent = continent.rstrip()
                    try:
                        d = Distribution.objects.get(continent=continent)
                        print(str(d) + " - Distribution already exists")
                    except ObjectDoesNotExist:
                        d = Distribution(continent=continent,
                                         distribution_gps_file_available=distribution_gps_file_available,
                                         distribution_area=distribution_area,
                                         exotic_countries=exotic_countries,
                                         invasive_countries=invasive_countries,
                                         country_state_native=country_state_native,
                                         country_state_invasive=country_state_invasive,
                                         IBRA_region=IBRA_region,
                                         IBRA_subregion=IBRA_subregion)
                        d.save()
                        print(str(d) + " - Distribution added to database")

                else:
                    print("Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " +
                          str(file1) + ": - Null value found for Distribution")
                    raise SystemExit(0)

            # Check if ';' separator is present in mine name, which would indicate that a species belongs to more than
            # one mine

            if ';' in mine_name and '.' not in mine_name and ',' not in mine_name:
                #print(mine_name + " - Species distribution across more than one mine " + '(' + str(mine_name) + ") "
                #                                                                                                "found")
                # ';' separator is present, so continue here
                # Split mine names based on ';'
                split_mine_names = mine_name.split(';')
                # Split mine latitudes based on ';'

                if ';' in mine_lat and ';' in mine_lng:
                    split_mine_lats = mine_lat.split(';')
                    # Split mine longitudes based on ';'
                    split_mine_lngs = mine_lng.split(';')
                    #print(split_mine_lats, split_mine_lngs, split_mine_names)
                    # Assert that the length of mine name(s) is the same as the length of mine latitudes and mine
                    # longitudes. If this is not true, report error.
                    assert len(split_mine_names) == len(split_mine_lats) \
                    and len(split_mine_names) == len(split_mine_lngs),\
                        "Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1) + \
                        ": Expected equal number of arguments for mine name(s), mine latitude(s) and mine " \
                        "longitude(s). Got {:d} mine names, {:d} mine latitudes and {:d} mine " \
                        "longitudes".format(index + 1, len(split_mine_names), len(split_mine_lats),
                         len(split_mine_lngs))

                    for mine, coord_lat, coord_lng in zip(split_mine_names, split_mine_lats, split_mine_lngs):
                        # Try get existing Mine objects or create a new one
                        try:
                            # Remove all white spaces from mine names and overwrite 'mine' variable
                            mine = mine.lstrip()
                            mine = mine.rstrip()
                            # Get list of all mine_name objects
                            m = Mine.objects.get(name=mine)
                            # Save and add MtoM relationship of species to mine
                            s.mines.add(m)
                            print(str(m) + " - Mine already exists")
                            print(str(s) + " - Added to mine " + str(m))
                        # Mine objects do not exist, so continue here
                        except ObjectDoesNotExist:
                            # Remove all white spaces from coordinates
                            coord_lat = coord_lat.replace(" ", "")
                            coord_lng = coord_lng.replace(" ", "")
                            # Load Mine objects mine_name, latitude and longitude
                            m = Mine(name=mine,
                                     # client=row[25],
                                     latitude=coord_lat,
                                     longitude=coord_lng)
                            # Save Mine objects
                            m.save()
                            print(str(m) + " - Mine added to database")
                            # Save and add MtoM relationship of species to mine
                            # s.mines.add(m)
                            print(str(s) + " - Added to mine " + str(m))

                # Check if fullstop(s) or comma(s) have been used as a delimiter for mine names
                elif ';' in mine_name and ';' not in mine_lat or ';' not in mine_lng:
                    # Fullstop(s) or comma(s) have been used incorrectly as delimiters, so report this
                    print("Error - Row number " + str(index+2) + " of " + str(sheet) + " from file " + str(file1) +
                          ": Mismatch in the number of mine name(s), mine latitude(s) and mine longitude(s)")
                    print("Hint: A missing semicolon in mine latitude or mine longitude might have "
                          "caused this error...")
                    # Terminate programme
                    raise SystemExit(0)

                elif ';' not in mine_name and ';' in mine_lat or ';' in mine_lng:
                    # Fullstop(s) or comma(s) have been used incorrectly as delimiters, so report this
                    print("Error - Row number " + str(index + 2) + " of " + str(sheet) + " from file " + str(file1) +
                          ": Mismatch in the number of mine name(s), mine latitude(s) and mine longitude(s)")
                    print("Hint: A missing semicolon in mine name might have caused this error...")
                    # Terminate programme
                    raise SystemExit(0)

                # Iterate over each mine name, latitude and longitude element in arrays

            elif '.' in mine_name or ',' in mine_name:
                # Fullstop(s) or comma(s) have been used incorrectly as delimiters, so report this
                print("Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1) +
                      ": Invalid delimiter used")
                print("Hint: A comma or period might have been used as a delimiter instead of a semicolon...")
                # Terminate programme
                raise SystemExit(0)

            else:
                # Create a list from mine name, mine latitude and mine longitude to check lists later for emptiness
                mine_list = [mine_name]
                lat_list = str(mine_lat)
                lng_list = str(mine_lng)

                try:
                    # Split mine names based on semicolon delimiter
                    # print(mine_list, lat_list, lng_list)
                    # print(len(mine_list), len(lat_list), len(lng_list))
                    # Split mine latitudes and longitudes based on semicolon
                    split_mine_lats = lat_list.split(';')
                    split_mine_lngs = lng_list.split(';')

                    # Check if mine names, latitude and longitude are not null
                    if split_mine_lats and split_mine_lngs and mine_list:
                        # Assert that the number of mines and clients are the same and, if not, report mismatch
                        assert len(mine_list) == len(split_mine_lats) and \
                               len(mine_list) == len(split_mine_lngs), "Error: Row number {:d} in sheet " + str(sheet) \
                               + " of file " + str(file1) + ": Expected equal number of arguments for mine names, "\
                               "mine latitude and mine longitude. Got {:d} mine name(s), {:d} mine latitude(s) and " \
                               "{:d} mine longitudes".format(
                                index+2, len(mine_list), len(split_mine_lats), len(split_mine_lngs))

                        # Try get existing Mine objects or create a new one
                        try:
                            m = Mine.objects.get(name=mine_name)
                            print(str(m) + " - Mine already exists")
                        except ObjectDoesNotExist:
                            m = Mine(name=mine_name,
                                     latitude=mine_lat,
                                     longitude=mine_lng)
                            # Save mine object
                            m.save()
                            # Report mine that has been added to database
                            print(str(m) + " - Mine added to database")

                    else:
                        # A null value entry has been found for mine name, mine latitude or mine longitude. Report error
                        # and exit program
                        print("Access database row " + str(index+1) + " - Null value(s) found for mine name, mine "
                                                                      "latitude or mine longitude")
                        raise SystemExit(0)
                # Mine names, latitudes or longitudes cannot be split, so report error
                except:
                    print("Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1)
                          + ": Expected equal number of arguments for mine name(s), latitudes and "
                            "longitudes")
                    # Terminate program
                    raise SystemExit(0)

            # Check if delimiters other than a semicolon is used in client_name
            if ';' in client_name and '.' not in client_name and ',' not in client_name:
                # One or more semicolons found in client_name
                print(client_name + " - Client operates more than one mine " + "(" + str(client_name) + ")" + " found")

                # Semicolon found, so split client names based on semicolon
                split_client_names = client_name.split(';')

                # Check if number of mines and clients matches
                if ';' in mine_name and '.' not in mine_name and ',' not in mine_name:
                    split_mine_names = mine_name.split(';')
                    # Assert that the number of mines and clients are the same based on semicolons
                    # and, if not, report mismatch
                    assert len(split_client_names) == len(split_mine_names), \
                        "Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1) + \
                        ": Expected equal number of arguments for client name(s) and mine name(s). Got {:d} client " \
                        "name(s) and {:d} mine name(s)".format(len(split_client_names), len(split_mine_names))

                # Iterate over each client element in client_name array
                for client in split_client_names:
                    # Try get existing client objects or create a new one
                    try:
                        # Remove all white spaces from client names and overwrite 'client' variable
                        client = client.replace(" ", "")
                        # Get list of all client_name objects
                        c = Client.objects.get(client_name=client)
                        # Save and add MtoO relationship of client to mine
                        c.save()
                        print(str(c) + " - Client already exists")
                    # Mine objects do not exist, so continue here
                    except ObjectDoesNotExist:
                        # Load Client objects client_name
                        c = Client(client_name=client)
                        # Save Client objects
                        c.save()
                        print(str(c) + " - Client added to database")
                        # Save and add MtoO relationship of client to mine
                        c.mines.add(m)
                        print(str(m) + " - Added to client " + str(c))

            # Check if fullstop or comma has been used as a delimiter for client names
            elif '.' in client_name or ',' in client_name:
                # Fullstop(s) or comma(s) have been used incorrectly as delimiters, so report this
                print("Error - Row number " + str(index) + " in sheet " + str(sheet) + " of file " + str(file1) +
                      ": Invalid delimiter used")
                print("Hint: A comma or period might have been used as a delimiter instead of a semicolon...")
                # Terminate program
                raise SystemExit(0)
            # For every other data entry scenario, continue here
            else:
                # Create a list from client name to check list later for emptiness
                client_list = [client_name]
                try:
                    # Split mine names based on semicolon delimiter
                    split_mine_names = mine_name.split(';')
                    # Check if mine names and client name are not empty
                    if split_mine_names and client_list:
                        # Assert that the number of mines and clients are the same and, if not, report mismatch
                        assert len(client_list) == len(split_mine_names), \
                            "Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1)\
                            + ":Expected equal number of arguments for client name(s) and mine name(s)"

                        try:
                            c = Client.objects.get(client_name=client_name)
                            print(str(c) + " - Client already exists")
                        except ObjectDoesNotExist:
                            c = Client(client_name=client_name)
                            c.save()
                            print(str(c) + " - Client added to database")

                    else:
                        print("Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " +
                              str(file1) + ": - Null value found for Client or Mine")
                        raise SystemExit(0)

                except:
                    print("Error - Row number " + str(index+2) + " in sheet " + str(sheet) + " of file " + str(file1)
                          + ": Expected equal number of arguments for client name(s) and mine name(s). Got {:d} client "
                            "name(s) and {:d} mine name(s)".format(len(client_list), len(split_mine_names)))
                    # Terminate program
                    raise SystemExit(0)

            # MtoM and MtoO relationships defined here
            d.species.add(s)
            print(str(d) + " - Distribution relation added to species " + str(s))

            c.mines.add(m)
            print(str(c) + " - Client relation added to mine " + str(m))

            s.mines.add(m)
            print(str(s) + " - Species relation added to mine " + str(m))

            h.species.add(s)
            print(str(h) + " - Habit relation added to species " + str(s))

            f.species.add(s)
            print(str(f) + " - Family relation added to species " + str(s))

            s.planting_windows.add(p)
            print(str(p) + " - Planting window relation added to specie " + str(s))

            # Show latest mine primary key
            latest_mine = Mine.objects.latest('pk').pk
            print("Most recent Mine entry pk: " + str(latest_mine))
            # Show latest species primary key
            latest_specie = Specie.objects.latest('pk').pk
            print("Most recent Specie entry pk: " + str(latest_specie))

        response = "\n---> Successfully completed loading database file!"
        return response

