from django.core.management.base import BaseCommand
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from species_db.models import Client, Specie, Mine, Planting_Window, Habit, Family, Distribution
from tqdm import tqdm
import time

class Command(BaseCommand):
    help = 'Get sample data from species database'

    def handle(self, **kwargs):
        """

        :param kwargs:
        :return: "---> All records deleted!" or "---> Operation cancelled by user!" depending on user input
        """

        print("Delete all records in database? Note: This action cannot be reversed...")
        confirmation = input("y/N ...")

        # Check if user answered in the affirmative

        if confirmation == 'y' or confirmation == 'Y':
            pbar = tqdm(total=100)
            #for i in range(1):

        # Check if Mine object exists and if so delete
            try:
                Mine.objects.all().delete()
                pbar.update(14.28)
                print("Mine objects deleted")
            except ObjectDoesNotExist:
                print("No records of Mine class found")
                pbar.update(14.28)

        # Check if Distribution object exists and if so delete
            try:
                Distribution.objects.all().delete()
                pbar.update(14.28)
                print("Distribution objects deleted")
            except ObjectDoesNotExist:
                print("No records of Distribution class found")
                pbar.update(14.28)

        # Check if Client object exists and if so delete
            try:
                Client.objects.all().delete()
                print("Client objects deleted")
                pbar.update(14.28)
            except ObjectDoesNotExist:
                print("No records of Client class found")
                pbar.update(14.28)

        # Check if Planting_Window object exists and if so delete
            try:
                Planting_Window.objects.all().delete()
                print("Planting Window objects deleted")
                pbar.update(14.28)
            except ObjectDoesNotExist:
                print("No records of Planting Window class found")
                pbar.update(14.28)
        # Check if Habit object exists and if so delete

            try:
                Habit.objects.all().delete()
                print("Habit objects deleted")
                pbar.update(14.28)
            except ObjectDoesNotExist:
                print("No records of Habit class found")
                pbar.update(14.28)

        # Check if Family object exists and if so delete
            try:
                Family.objects.all().delete()
                print("Family objects deleted")
                pbar.update(14.28)
            except ObjectDoesNotExist:
                print("No records of Family class found")
                pbar.update(14.28)

        # Check if Specie object exists and if so delete
            try:
                Specie.objects.all().delete()
                print("Species objects deleted")
                pbar.update(14.28)
            except ObjectDoesNotExist:
                print("No records of Species class found")
                pbar.update(14.28)

            print("---> All records deleted!")
            #pbar.close()

    # Check if user responded in the negative
        else:
            print("---> Operation cancelled by user")

    # for i in tqdm.tqdm(range(6)):
    #     time.sleep(0)
