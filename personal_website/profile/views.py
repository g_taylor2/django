from django.shortcuts import render, get_object_or_404, redirect
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.mail import EmailMessage
from django.urls import reverse
from django.conf import settings

from django.core.paginator import Paginator

from .forms import ContactForm
from . import views, urls, models
from .models import Entry

import os

# Create your views here.
def home(request):
    return render(request, 'profile/home.html')

def photography(request):
    # insert the path to your directory
    path = "G:\\All Pics & Videos\\Witney"
    img_list = os.listdir(path)
    # return render_to_response('gallery.html', {'images': img_list})
    return render(request, 'profile/photography.html', {'images': img_list})

def about(request):
    return render(request, 'profile/about.html')

def contact(request):
    return render(request, 'profile/contact.html')#, context)

def email(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['taylor.graham@zohomail.eu'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('success')
    return render(request, 'profile/email.html', {'form': form})

def success(request):
    return render(request, 'profile/success.html')

def blog(request):
    posts = Entry.objects.all()
    paginator = Paginator(posts, 3)
    page = request.GET.get('page')
    #?page=2
    posts = paginator.get_page(page)

    return render(request, 'profile/blog.html', {'posts': posts})

def blog_detail(request, slug):
    post = get_object_or_404(Entry, slug=slug)
    return render(request, 'profile/blog_detail.html', {'post': post})
