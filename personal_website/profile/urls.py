"""personal_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'profile'

urlpatterns = [
    # /profile/
    path('', views.home, name='home'),

    # /profile/photography/
    path('photography/', views.photography, name='photography'),

    # /profile/about/
    path('about/', views.about, name='about'),

    # /profile/contact/
    path('contact/', views.contact, name='contact'),

    path('email/', views.email, name='email'),

    path('email/success/', views.success, name='success'),

    path('blog/', views.blog, name='blog'),

    path('blog/<slug>', views.blog_detail, name='blog_detail')

]

